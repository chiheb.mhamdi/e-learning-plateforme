from django.db import models

# Create your models here.
class Formation(models.Model) : 
    titre = models.CharField(max_length=100) 
    logo = models.ImageField(upload_to='pics')
    etat = models.BooleanField(default=False)
    choix_categorie=(
        ('Web','Web'),
        ('Mobile','Mobile'),
        ('Cloud','Cloud')
    )
    categorie=models.CharField(max_length=30,blank=True,null=False,choices=choix_categorie)
    