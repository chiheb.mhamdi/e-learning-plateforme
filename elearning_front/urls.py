from django.urls import path
from . import views 
urlpatterns = [
    path('',views.index,name='index'),
    path('formations',views.formations,name='formations'),
    path('recherche',views.recherche,name='recherche'),
    path('formation_details/<int:formation_id>/',views.formation_details,name='formation_details')
]
