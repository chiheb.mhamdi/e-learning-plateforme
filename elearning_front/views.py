from django.shortcuts import render,redirect
from .models import Formation 
from django.contrib import messages 


# Create your views here.

def index(request): 
    formations=Formation.objects.all()[:3]
    return render(request,'index.html',{'formations':formations})

def formations(request) :
    formations=Formation.objects.all()
    return render(request,'formations.html',{'formations':formations})

def recherche(request) : 
    if request.method == 'POST' :
        nom_formation=request.POST['nom_formation']
        categorie_formation=request.POST['categorie']
        formations=Formation.objects.all().filter(categorie=categorie_formation)
        if not formations : 
            messages.info(request,"Cette formation n'est pas disponible")
            return render(request,'formation_recherchee.html')
        else  : 
             return render(request,'formation_recherchee.html',{'formations':formations})
    return render(request,'rechercheFormation.html') 
def formation_details(request,formation_id):
    formation=Formation.objects.filter(id=formation_id)
    return render(request, 'formation_details.html',{'formations': formation})